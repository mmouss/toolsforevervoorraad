from math import prod
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from .models import Location, Orderlist, Product, Stock
from django.contrib import messages


def home(request, location=None, product=None):
    if not request.user.is_authenticated:
        return redirect('/login')
    if product is not None:
        context = {
            'username': request.session.get('username'),
            'locations': list(Location.objects.all()),
            'products': list(Product.objects.all()),
            'selected_location': Location.objects.filter(id=location).first(),
            'results': Stock.objects.filter(
                location__id=location, product__id=product)
        }
    elif location is not None:
        context = {
            'username': request.session.get('username'),
            'locations': list(Location.objects.all()),
            'products': list(Product.objects.all()),
            'selected_location': Location.objects.filter(id=location).first(),
            'results': Stock.objects.filter(
                location__id=location)
        }
    else:
        context = {
            'username': request.session.get('username'),
            'locations': list(Location.objects.all()),
            'products': list(Product.objects.all())
        }

    return render(request, 'home.html', context)


def locations(request):
    response = Location.objects.all()
    return HttpResponse(response)


def login_view(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.POST:
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            request.session['username'] = username
            return redirect('/')
        else:
            return redirect('/login')
    return render(request, 'loginpage.html')


def logout_view(request):
    logout(request)
    return redirect('/login')
