from django.db import models


class Location(models.Model):
    adress = models.CharField(max_length=105, verbose_name='Adres')
    city = models.CharField(max_length=105, verbose_name='Stadsplaats')

    def __str__(self):
        return str(self.adress) + ' , ' + self.city


class Product(models.Model):
    Location = models.ManyToManyField(Location, verbose_name='locatie')
    brand = models.CharField(max_length=105, verbose_name='Merk')
    type = models.CharField(max_length=105, verbose_name='Model')
    factory = models.CharField(max_length=105, verbose_name='Fabriek')
    inkoopprijs = models.DecimalField(
        max_digits=10, decimal_places=2, null=True)
    verkoopprijs = models.DecimalField(
        max_digits=10, decimal_places=2, null=True)

    def __str__(self):
        return str(self.brand) + ' , ' + self.type


class OrderProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.RESTRICT)
    aantal = models.IntegerField(verbose_name="Aantal")

    def __str__(self):
        return str(self.aantal) + 'x ' + self.product.type


class Orderlist(models.Model):
    bestellingdatum = models.DateField()
    ordername = models.CharField(
        max_length=105, verbose_name='Bestellingnaam', null=True)
    Location = models.ForeignKey(
        Location, null=True, on_delete=models.RESTRICT)
    products = models.ManyToManyField(OrderProduct, verbose_name="Producten")

    def __str__(self):
        return "%s %s" % (self.ordername, self.bestellingdatum)


class Stock(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    aantal = models.IntegerField(verbose_name="Vooraad")

    def __str__(self):
        return str(self.aantal) + ' - ' + self.product.brand + ' - ' + self.location.adress + ', ' + self.location.city
