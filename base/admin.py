from django.contrib import admin
from .models import Location, Product, Orderlist, OrderProduct, Stock

admin.site.register(Location)
admin.site.register(Product)
admin.site.register(Orderlist)
admin.site.register(OrderProduct)
admin.site.register(Stock)
