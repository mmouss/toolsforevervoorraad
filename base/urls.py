from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('<int:location>', views.home),
    path('<int:location>/<int:product>', views.home),
    path('locations', views.locations),
    path('login', views.login_view),
    path('logout', views.logout_view),
]
