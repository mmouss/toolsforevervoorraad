# Generated by Django 4.0.4 on 2022-06-18 15:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0012_stock_aantal'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='type',
            new_name='product_type',
        ),
    ]
