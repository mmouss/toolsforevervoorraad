# Generated by Django 4.0.4 on 2022-06-18 12:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_orderlist_ordername'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='aantal',
        ),
        migrations.RemoveField(
            model_name='orderlist',
            name='Location',
        ),
        migrations.AddField(
            model_name='orderlist',
            name='Location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, to='base.location'),
        ),
        migrations.RemoveField(
            model_name='orderlist',
            name='Product',
        ),
        migrations.AddField(
            model_name='orderlist',
            name='Product',
            field=models.ManyToManyField(to='base.product', verbose_name='Products'),
        ),
        migrations.AlterField(
            model_name='product',
            name='type',
            field=models.CharField(max_length=105, verbose_name='Model'),
        ),
    ]
